(function(){
// Ionic Starter App
'use strict';
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var injections = ['$rootScope','$ionicPlatform','$ionicModal','ENV','baseFunctions','ngFB','$analytics'];
var runner = function($rootScope,$ionicPlatform,$ionicModal,ENV,baseFunctions,ngFB,$analytics) {
  $rootScope.getStore = false;
  ngFB.init({appId: '1523498517919967'});
  $rootScope.pageInProgress=true;
  $ionicPlatform.ready(function() {
    //console.log($analytics)
    //$analytics.startTrackerWithId('UA-57701167-1');
  });
  var sessionId = localStorage.getItem('sessionId'),model={sessionId:null};
  if(sessionId){
    model.sessionid = sessionId;
  }
  baseFunctions.connect(model).success(function(response,status,headers){
    if(response.sessionid){
      $rootScope.user = response.user;
      localStorage.setItem('sessionId',response.sessionid);
    }
    baseFunctions.mainData().success(function(response){
      baseFunctions.chacheMainData = response;
      $rootScope.pageInProgress = false;
      $rootScope.config = response;
    });
    $ionicPlatform.ready(function() {

      if(window.cordova && window.cordova.plugins.Keyboard) {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        // Don't remove this line unless you know what you are doing. It stops the viewport
        // from snapping when text inputs are focused. Ionic handles this internally for
        // a much nicer keyboard experience.
        cordova.plugins.Keyboard.disableScroll(true);
      }

      if(window.StatusBar) {
        StatusBar.styleDefault();
      }
    });

    //ROOT FILTERS
    $rootScope.checkStore = function(g){
      return localStorage.store === 'male'&&g.gender.male||localStorage.store === 'female'&&g.gender.female
    };
    //END ROOT FILTERS

    $rootScope.$on('$stateChangeSuccess',function(){
      //  $analytics.pageTrack(location.href);
    });

  }).error(function(){

  });
};
runner.$inject = injections;
angular.module('takibuApp', ['ionic','config','ionic-img-lazy-load','ngOpenFB','angulartics', 'angulartics.google.analytics'])
.config(function ($analyticsProvider) {
  // turn off automatic tracking
  // $analyticsProvider.virtualPageviews(false);
})
  .run(runner)
}())
