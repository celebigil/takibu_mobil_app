(function(){
	var injections = ['$http','ENV']
	var pageService = function($http,ENV){
		var resource = {};

		resource.pullPage = function(model){
			return $http.post(ENV.apiEndpoint + 'sys_mobile/page',model);
		}

		return resource;

	};

	pageService.$injec = injections;

	angular.module('takibuApp').service('pageService',pageService);

}());
