'use strict';
(function() {
  var injections = ['$scope','$rootScope', 'homePageFactory', '$ionicModal'];
  var homePageController = function($scope,$rootScope, homePageFactory, $ionicModal) {
    homePageFactory.pullHomePage().success(function(response) {
        $scope.home = response;
    });

    if (localStorage.store) {
      $scope.store = localStorage.store;
    } else {
      $ionicModal.fromTemplateUrl('homepage/modal.html', {
      }).then(function(modal) {
        $scope.modal = modal;
        $scope.modal.show();
      });
      $scope.getStore = true;
      $rootScope.selectStore = function(s) {
        localStorage.store = s;
        $scope.getStore = false;
        $scope.modal.hide();
      }
    }

  };

  homePageController.$inject = injections;

  angular.module('takibuApp').controller('homePageController', homePageController);

}())
