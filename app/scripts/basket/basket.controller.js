'use strict';
(function() {
  var injections = ['$scope','$rootScope', 'basketService', '$ionicModal'];
  var basketController = function($scope,$rootScope, basketService, $ionicModal) {

  };

  basketController.$inject = injections;

  angular.module('takibuApp').controller('basketController', basketController);

}())
