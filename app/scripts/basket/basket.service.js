(function(){
	var injections = ['$http','ENV']
	var basketService = function($http,ENV){
		var resource = {};

		resource.basketAdd = function(model){
			model.sessionid=localStorage.getItem('sessionId');
			return $http.post(ENV.apiEndpoint + 'sys_mobile/basket_add',model);
		};

		resource.basketRemove = function(model){
			model.sessionid=localStorage.getItem('sessionId');
			return $http.post(ENV.apiEndpoint + 'sys_mobile/basket_remove',model);
		};

		resource.basketList = function(){
			return $http.post(ENV.apiEndpoint + 'sys_mobile/basket_list',{'sessionid':localStorage.getItem('sessionId')});
		};

		return resource;

	};

	basketService.$injec = injections;

	angular.module('takibuApp').service('basketService',basketService);

}());
