(function(){
	var injections = ['$http','ENV','$q','basketService','$rootScope']
	var basketFactory = function($http,ENV,$q,basketService, $rootScope){
		var resource = {};

		resource.basketList = function(){
			var promise = $q.defer();
			basketService.basketList().success(function(res){
				$rootScope.products = res.products;
				$rootScope.freeShipping = res.free_shipping;
				promise.resolve(res);
			}).error(function(res){
				promise.reject(res);
			});
			return promise.promise;
		};

		return resource;

	};

	basketFactory.$injec = injections;

	angular.module('takibuApp').service('basketFactory',basketFactory);

}());
