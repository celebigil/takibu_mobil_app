(function(){
  'use strict';

  var injector = ['ENV','$http'];
  var userService = function(ENV,$http){
    var resource = {}
    resource.login = function(model){
      model.sessionid = localStorage.getItem('sessionId');
      return $http.post(ENV.apiEndpoint + 'sys_mobile/userConnect',model);
    };
    return resource;
  };

  userService.$inject = injector;
  angular.module('takibuApp').service('userService',userService);
})();
