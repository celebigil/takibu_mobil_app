(function(){
  'use strict';

  var injector = ['$rootScope','userService','$q'];
  var userService = function($rootScope,userService,$q){
    var resource = this;
    resource.login = function(model){
      var defferer = $q.deffer();
      defferer.resolve({
        mail:'budamivardi@mail.com',
        name:'kamuran',
        surname:'sönecek',
        ip:'127.0.0.1',
        lastLoggin:Date.now(),
        registiration:'On gün önce'
      });
       /*userService.login(model).success(function(response){
        $rootScope.loggedIn = true;
        resource.userInformations = response;
        defferer.resolve(response);
      }).error(function(response){
        $rootScope.loggedIn = false;
        defferer.reject(response);
      });*/
      return defferer.promise;
    };
    return resource;
  };

  userService.$inject = injector;
  angular.module('takibuApp').service('userService',userService);
})();
