(function(){
	var injections = ['$http','ENV']
	var homePageFactory = function($http,ENV){
		var resource = {};

			console.log(1,ENV);
		resource.pullHomePage = function(){
			return $http.get(ENV.apiEndpoint + 'sys_mobile/homepage');
			// return $http.get('http://www.birimhesapla.com/homepage.json');
		}

		return resource;

	};

	homePageFactory.$injec = injections;

	angular.module('takibuApp').factory('homePageFactory',homePageFactory);

}());
