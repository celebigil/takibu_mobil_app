'use strict';
(function() {
  var injections = ['$scope','$rootScope', 'homePageFactory', '$ionicModal', 'baseFunctions'];
  var pageController = function($scope,$rootScope, homePageFactory, $ionicModal, baseFunctions) {
    var model = {
      url : $state.params.url
    }
    homePageFactory.pullPage(model).success(function(response) {
      $scope.html = $sce.trustAsHtml(response);
    });

  

  };

  pageController.$inject = injections;

  angular.module('takibuApp').controller('pageController', pageController);

}())
