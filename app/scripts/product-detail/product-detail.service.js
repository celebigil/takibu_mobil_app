(function(){
	'use strict';
	var injections = ['$http','ENV'];
	var productDetailService = function($http,ENV){
		var resource = {};
		resource.pullProductDetail = function(url){
			return $http.post(ENV.apiEndpoint + 'sys_mobile/productDetail',{seo:url});
		};
		return resource;
	};

	productDetailService.$injec = injections;

	angular.module('takibuApp').service('productDetailService',productDetailService);
}());
