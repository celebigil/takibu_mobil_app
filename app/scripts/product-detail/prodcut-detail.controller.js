(function(){
	'use strict';
	var injections = ['$scope','productDetailService','$state','$sce','baseFunctions','basketService','basketFactory', '$rootScope'];
	var productDetailController = function($scope,productDetailService,$state,$sce,baseFunctions,basketService,basketFactory,$rootScope){

		$scope.model = {
			quantity:1
		};

		productDetailService.pullProductDetail($state.params.url).success(function(response){
			$scope.product = response;
			$scope.product.description = $sce.trustAsHtml(response.description);
		});

		$scope.shipment_time = $sce.trustAsHtml(baseFunctions.chacheMainData.kargo_sure);
		$scope.shipment_price = $sce.trustAsHtml(baseFunctions.chacheMainData.kargo_fiyat);
		$scope.recived_info = $sce.trustAsHtml(baseFunctions.chacheMainData.teslimat_bilgi);
		

		$scope.addBasket = function(){
			var model = {
				cart_product_id:$scope.product.id,
				cart_quantity:$scope.model.quantity
			};
			$rootScope.pageInProgress = true;
			basketService.basketAdd(model).success(function(response){
				$rootScope.cartTotal = response.carttotal;
				basketFactory.basketList().then(function(response2){
					$rootScope.toggleRight();
					$rootScope.pageInProgress = false;
				});
			}).error(function(){
				$rootScope.pageInProgress = false;
			});
		};

	};

	productDetailController.$inject = injections;

	angular.module('takibuApp').controller('productDetailController',productDetailController);

}());
