'use strict';
(function() {
  var injections = ['$scope','$rootScope', 'homePageFactory', '$ionicModal', 'baseFunctions','$state'];
  var userSigninCtrl = function($scope,$rootScope, homePageFactory, $ionicModal, baseFunctions, $state) {
    console.log($rootScope.user);
    $scope.$on('userLoggedIn',function(){
      $state.go('selectAdress');
    });
  };

  userSigninCtrl.$inject = injections;

  angular.module('takibuApp').controller('userSigninCtrl', userSigninCtrl);

}())
