'use strict';
(function() {
  var injections = ['$scope','$rootScope', 'homePageFactory', '$ionicModal', 'baseFunctions'];
  var userController = function($scope,$rootScope, homePageFactory, $ionicModal, baseFunctions) {
    console.log($rootScope.user);
    $scope.saat = Date.now();
  };

  userController.$inject = injections;

  angular.module('takibuApp').controller('userController', userController);

}())
