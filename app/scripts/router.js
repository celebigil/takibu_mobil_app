(function(){

'use strict';
var injections = ['$stateProvider','$urlRouterProvider','$locationProvider','$httpProvider'];
var router = function($stateProvider, $urlRouterProvider,$locationProvider,$httpProvider) {
	$urlRouterProvider.otherwise('/');
	$httpProvider.interceptors.push('requestInterceptor');
	// $httpProvider.defaults.withCredentials = true;
	//$locationProvider.html5Mode(true);

  	$stateProvider.state('home', {
    	url: '/',
    	templateUrl: 'templates/home/home.html',
    	controller:'homePageController'
  	});

		$stateProvider.state('category', {
	    	url: '/category/:categoryName',
	    	templateUrl: 'templates/category/category.html',
	    	controller:'categoryPageController'
	  	});

			$stateProvider.state('productDetail',{
				url: '/productDetail/:url',
				templateUrl: 'templates/product-detail/product-detail.html',
				controller:'productDetailController'
			});

			$stateProvider.state('pageDetail',{
				url: '/pages/:url',
				templateUrl: 'templates/pages/pages.html',
				controller:'pageService'
			});

			/*User Pages*/

			$stateProvider.state('userDetail',{
				url: '/user/detail',
				templateUrl: 'templates/user/detail.html',
				controller:'userController'
			});

			$stateProvider.state('user.myOrder',{
				url: '/user/my_order',
				templateUrl: 'templates/user/myOrder.html',
				controller:'myOrder'
			});

			$stateProvider.state('user.coupons',{
				url: '/user/my_coupons',
				templateUrl: 'templates/user/coupons.html',
				controller:'pageService'
			});

			$stateProvider.state('user.tickets',{
				url: '/user/my_tickets',
				templateUrl: 'templates/user/tickets.html',
				controller:'tickets'
			});

			$stateProvider.state('user.profile',{
				url: '/user/:url',
				templateUrl: 'templates/user/user.html',
				controller:'pageService'
			});

			$stateProvider.state('user.adresses',{
				url: '/pages/:url',
				templateUrl: 'templates/pages/pages.html',
				controller:'pageService'
			});

			$stateProvider.state('user.security',{
				url: '/pages/:url',
				templateUrl: 'templates/pages/pages.html',
				controller:'pageService'
			});

			$stateProvider.state('basket',{
				url: '/sepet',
				templateUrl: 'templates/basket/basket.html',
				controller:'basketController'
			});

			$stateProvider.state('basketCheckout',{
				url: 'sepet/adresler',
				templateUrl: 'templates/basket/basket_adresses.html',
				controller:'basketAdressesController'
			});

			$stateProvider.state('userSignIn',{
				url: '/giris',
				templateUrl: 'templates/user/user_signin.html',
				controller:'userSigninCtrl'
			});

			/* end of user details*/

};
router.$inject = injections;
angular.module('takibuApp').config(router);
}())
