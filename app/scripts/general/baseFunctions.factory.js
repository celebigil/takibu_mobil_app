(function(){
  'use strict';
  var injections = ['$http', 'ENV'];
  var baseFunctions = function($http, ENV) {
    var resource = {};
    resource.connect = function(model) {
      // return $http.get('http://www.birimhesapla.com/menu.json');
      return $http.post(ENV.apiEndpoint + 'sys_mobile/connect',model);
    };

    resource.fconnect = function(model) {
      // return $http.get('http://www.birimhesapla.com/menu.json');
      return $http.post(ENV.apiEndpoint + 'sys_mobile/fconnect',model);
    };

    resource.menu = function(){
      // return $http.get('http://www.birimhesapla.com/menu.json');
      return $http.get(ENV.apiEndpoint +'sys_mobile/menu');
    };

    resource.mainData = function(){
      return $http.get(ENV.apiEndpoint + 'sys_mobile/main_data');
    };

    return resource;
  };

  baseFunctions.$inject = injections;

  angular.module('takibuApp').factory('baseFunctions', baseFunctions);
}());
