(function(){
  'use strict';
  var injector = [];
  var requestInterceptor  = function(){
    return {
      request: function(config){
        if(localStorage.getItem('takibuToken')){
          console.log('takibu',config);
          config.headers['takibu'] = localStorage.getItem('takibuToken');
        }
        return config;
      }
    };
  };
  requestInterceptor.$inject  = injector;
angular.module('takibuApp').factory('requestInterceptor',requestInterceptor);
})();
