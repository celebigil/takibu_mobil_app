'use strict';
(function(){
	var injections = ['$scope','categoryPageFactory','$state'];
	var categoryPageController = function($scope,categoryPageFactory,$state){
		$scope.startIndex = 0;
		$scope.endIndex = 47;
		$scope.currentPage = 1;
		$scope.pages =[];

		$scope.changePage = function(page){
			$scope.startIndex = (page - 1)*48;
			$scope.endIndex = page*48 - 1;
			$scope.currentPage = page;
			$('body,html').animate({scrollTop:0},500);
		};

		var model = {
			url : $state.params.categoryName
		};

		categoryPageFactory.pullCategory(model).success(function(response){
			$scope.category = response;
			for(var i=0;i<=(response.productCount / 48);i++){
				$scope.pages.push(i+1);
			}
		});
	};

	categoryPageController.$inject = injections;

	angular.module('takibuApp').controller('categoryPageController',categoryPageController);

}());
