(function(){
	var injections = ['$http','ENV']
	var categoryPageFactory = function($http,ENV){
		var resource = {};

		resource.pullCategory = function(model){
			return $http.post(ENV.apiEndpoint + 'sys_mobile/category',model);
			// return $http.get('http://www.birimhesapla.com/categories.json');
		};

		return resource;

	};

	categoryPageFactory.$injec = injections;

	angular.module('takibuApp').factory('categoryPageFactory',categoryPageFactory);

}());
