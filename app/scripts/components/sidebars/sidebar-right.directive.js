(function(){
'use strict';
	
	var injections = [];
	var sideBarRight = function(){
		return {
			'rescrict':'E',
			'replace':true,
			'controller':'sideBarRightController',
			'templateUrl':'/templates/components/sidebars/sidebarright.html'
		};
	};

	sideBarRight.$inject = injections;

	angular.module('takibuApp').directive('sideBarRight',sideBarRight);

}());