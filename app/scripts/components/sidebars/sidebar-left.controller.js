(function(){
	'use strict';
	var injections = ['$scope','baseFunctions']
	var sideBarLeftController = function($scope,baseFunctions){
		baseFunctions.menu().success(function(response){
			$scope.menus = response.menus;
		}).error(function(response){
			console.warn('Menü yüklenemedi...');
		});

		$scope.activeSubmenu = function(menu){
			menu.active = !menu.active;
		};

	};

	sideBarLeftController.$injec = injections;

	angular.module('takibuApp').controller('sideBarLeftController',sideBarLeftController);
}());
