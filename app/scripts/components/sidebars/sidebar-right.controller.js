(function(){
	'use strict';
	var injections = ['basketFactory','$scope','basketService','$rootScope','$ionicSideMenuDelegate']
	var sideBarRightController = function(basketFactory,$scope,basketService,$rootScope,$ionicSideMenuDelegate){
		var basketList = function(){
			basketFactory.basketList().then(function(response){
				console.log(response)
				$rootScope.basketList = response;
			});
		};
		basketList();

		$scope.removeBasket = function(id){
			var model = {
				cart_product_row_id:id,
				cart_quantity:0
			};
			basketService.basketRemove(model).success(function(response){
				$rootScope.cartTotal = response.carttotal;
				setTimeout(function(){
					basketList();
				},150);
			}).error(function(){

			});
		};

		$scope.$on('$stateChangeSuccess',function(){
			// $rootScope.toggleRight();
			console.log(($ionicSideMenuDelegate));
			$ionicSideMenuDelegate.toggleRight(false);
		});

	};

	sideBarRightController.$injec = injections;

	angular.module('takibuApp').controller('sideBarRightController',sideBarRightController);
}());
