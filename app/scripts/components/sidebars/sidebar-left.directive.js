(function(){
'use strict';
	
	var injections = [];
	var sideBarLeft = function(){
		return {
			'rescrict':'E',
			'replace':true,
			'controller':'sideBarLeftController',
			'templateUrl':'/templates/components/sidebars/sidebarleft.html'
		};
	};

	sideBarLeft.$inject = injections;

	angular.module('takibuApp').directive('sideBarLeft',sideBarLeft);

}());