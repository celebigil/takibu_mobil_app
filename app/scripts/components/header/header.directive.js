(function(){
	'use strict';
	var injections = []
	var ngHeader = function(){
		return {
			restrict:'E',
			controller:'headerController',
			templateUrl:'templates/components/headers/header.html'	
		}
	};

	ngHeader.$injec = injections;

	angular.module('takibuApp').directive('ngHeader',ngHeader);
}());