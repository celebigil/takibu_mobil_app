'use strict';
(function(){
	var injections = ['$scope','$ionicSideMenuDelegate','$ionicModal','$state','userService','$rootScope','ngFB','baseFunctions'];
	var headerController = function($scope,$ionicSideMenuDelegate,$ionicModal, $state,userService,$rootScope,ngFB,baseFunctions){
		/* user login model */

		$scope.sIModel = {
			email :'',
			phone:'',
			password:''
		};

		$scope.activeTab = 1;

		$scope.changeTab = function(t){
			$scope.activeTab = t;
		}


		$scope.toggleLeft = function(){
			$ionicSideMenuDelegate.toggleLeft();
		};

		$rootScope.toggleRight = function(){
			$ionicSideMenuDelegate.toggleRight();
		};

		$rootScope.toggleSignIn = function(){
			if($rootScope.user){
				$state.go('userDetail');
				return;
			};
			if($scope.signInModal){
					if($scope.signInModal._isShown){
						$scope.signInModal.hide();
					} else{
						$rootScope.loginModal = true;
						$scope.signInModal.show();
					}
					return;
			}
			$ionicModal.fromTemplateUrl('header/login.html', {
				scope:$scope
			}).then(function(modal){
				$rootScope.loginModal = true;
				$scope.signInModal = modal;
				$scope.signInModal.show();
			});
		};

		$scope.$on('modal.hidden',function(){
			$rootScope.loginModal = false;
		});


		$scope.signIn = function(){
			var model = {
				username:$scope.activeTab ===2 ? $scope.sIModel.phone + '@takibu.com' : $scope.sIModel.email,
				password:$scope.sIModel.password
			};

			userService.login(model).success(function(response){
				if(!response.error){
					response.user&&response.user.user_token&&localStorage.setItem('takibuToken',response.user.user_token);
					$scope.signInModal.hide();
					$rootScope.user = response.user;
				}
			})

		};
		// signIn
		// ngFB.logout(function(response) {
		//   // user is now logged out
		// 	console.log('response',response)
		// });

		$scope.fcSignIn = function () {
	    ngFB.login({scope: 'email',return_scopes:true}).then(
	      function (response) {
	        if (response.status === 'connected') {
							ngFB.api({path:'/me',params:{fields:'id,name,email'}}).then(function(response2) {
								console.log(response,response2);
								baseFunctions.fconnect({facebookid:response2.id,email:response2.email,access_token:response.authResponse.accessToken,sessionid:localStorage.getItem('sessionId')}).then(function(res){
									$rootScope.user = res.user;
								})
				      //  console.log('Good to see you, ' + response.name + '.');
				     });

							$scope.signInModal.hide();
	        } else {
	            alert('Facebook login failed');
	        }
	  	});
		};


	};

	headerController.$inject = injections;

	angular.module('takibuApp').controller('headerController',headerController);

}())
