(function(){
  var injections  = [];

  var categoryProduct = function(){
    return {
      restrict    :'A',
      templateUrl :'templates/components/product/product.html',
      controller  :'productController',
      scope : {
        product:'='
      }
    }
  }

  categoryProduct.$inject = injections;
  angular.module('takibuApp').directive('categoryProduct',categoryProduct);

}());
